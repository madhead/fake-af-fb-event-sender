plugins {
    java
    application
}

repositories {
    jcenter()
}

dependencies {
    implementation("org.apache.httpcomponents:httpclient:4.5.8")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.9.8")
    implementation("org.apache.logging.log4j:log4j-core:2.11.2")
    implementation("org.apache.logging.log4j:log4j-jcl:2.11.2")
}

application {
    mainClassName = "com.banuba.banuba.analytics.fakey.App"
}
