package com.banuba.banuba.analytics.fakey;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;

import java.net.URI;
import java.util.Optional;

public class App {
    public static void main(String[] args) throws Exception {
        final String app = Optional
                .ofNullable(System.getenv("FAKE_APP"))
                .orElseThrow(
                        () -> new IllegalArgumentException("FAKE_APP env variable is required")
                );
        final String token = Optional
                .ofNullable(System.getenv("FAKE_TOKEN"))
                .orElseThrow(
                        () -> new IllegalArgumentException("FAKE_TOKEN env variable is required")
                );
        final ObjectMapper objectMapper = new ObjectMapper();
        final ObjectNode event = (ObjectNode) objectMapper.readTree(App.class.getResourceAsStream("event.json"));
        final ObjectNode eventValue = (ObjectNode) objectMapper.readTree(App.class.getResourceAsStream("eventValue.json"));

        event.put("bundle_id", app);
        event.put("app_name", app);
        event.put("eventValue", eventValue.toString());

        final HttpClient client = HttpClients.createDefault();
        final HttpPost request = new HttpPost();

        request.setURI(URI.create("https://api2.appsflyer.com/inappevent/" + app));
        request.setHeader("Authentication", token);
        request.setHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType());
        request.setEntity(
                new StringEntity(event.toString())
        );

        final HttpResponse response = client.execute(request);

        System.out.println(response);
    }
}
